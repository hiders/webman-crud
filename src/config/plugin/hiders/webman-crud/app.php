<?php
return [
    'enable' => true, //是否启用

    'controller_dir' => 'app/controller', //控制器目录
    'model_dir'      => 'app/model', //模型目录

    'base_controller' => 'Hiders\WebmanCrud\Base\Controller', //控制器基类
    'base_model'      => 'Hiders\WebmanCrud\Base\Model', //模型基类

    'model_type' => 'laravel', //模型类型，laravel或thinkphp
];