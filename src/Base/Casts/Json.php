<?php

namespace Hiders\WebmanCrud\Base\Casts;

use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Contracts\Database\Eloquent\CastsInboundAttributes;

class Json implements CastsAttributes
{
    /**
     * @param        $model
     * @param string $key
     * @param        $value
     * @param array  $attributes
     *
     * @return array|null
     */
    public function get($model, string $key, $value, array $attributes): ?array
    {
        return $value ? json_decode($value, true) : null;
    }

    /**
     * @param        $model
     * @param string $key
     * @param        $value
     * @param array  $attributes
     *
     * @return false|string
     */
    public function set($model, string $key, $value, array $attributes): bool|string
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}