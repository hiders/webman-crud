<?php

namespace Hiders\WebmanCrud\Base\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Comma implements CastsAttributes
{
    /**
     * @param        $model
     * @param string $key
     * @param        $value
     * @param array  $attributes
     * @return string[]
     */
    public function get($model, string $key, $value, array $attributes): array
    {
        return $value ? explode(',', $value) : [];
    }

    /**
     * @param        $model
     * @param string $key
     * @param        $value
     * @param array  $attributes
     * @return string
     */
    public function set($model, string $key, $value, array $attributes): string
    {
        return is_array($value) ? implode(',', $value) : $value;
    }
}