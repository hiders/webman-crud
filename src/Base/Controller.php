<?php

namespace Hiders\WebmanCrud\Base;

use support\Response;

class Controller
{
    /** @var \Hiders\WebmanCrud\Base\Model */
    protected Model $model;

    protected array $fields = ['*'];

    protected array $searchFields = [];

    protected array $appendField = [];

    protected array $relation = [];
    protected array $joins = [];

    protected array $extWhere = [];
    protected array $extOrWhere = [];
    protected string $extOrderBy = '';

    protected array $selectPageOrderBy = [];

    /**
     * 操作成功返回的数据
     * @param string      $msg    提示信息
     * @param mixed|null  $data   要返回的数据
     * @param int         $code   错误码，默认为1
     * @param string|null $type   输出类型
     * @param array       $header 发送的 Header 信息
     */
    protected function success(string $msg = '', mixed $data = null, int $code = 1, ?string $type = null, array $header = []): Response
    {
        if (empty($msg)) {
            $msg = '操作成功';
        }

        return $this->result($msg, $data, $code, $type, $header);
    }

    /**
     * 操作失败返回的数据
     * @param string      $msg    提示信息
     * @param mixed|null  $data   要返回的数据
     * @param int         $code   错误码，默认为0
     * @param string|null $type   输出类型
     * @param array       $header 发送的 Header 信息
     */
    protected function error(string $msg = '', mixed $data = null, int $code = 0, ?string $type = null, array $header = []): Response
    {
        return $this->result($msg, $data, $code, $type, $header);
    }

    /**
     * 返回封装后的 API 数据到客户端
     * @access protected
     * @param string      $msg    提示信息
     * @param mixed|null  $data   要返回的数据
     * @param int         $code   错误码，默认为0
     * @param string|null $type   输出类型，支持json/jsonp
     * @param array       $header 发送的 Header 信息
     * @return Response
     */
    protected function result(string $msg, mixed $data = null, int $code = 0, ?string $type = null, array $header = []): Response
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data,
            'time' => time(),
        ];
        // 如果未设置类型则自动判断
        $type = $type ?: (request()->input('callback') ? 'jsonp' : 'json');

        switch ($type) {
            case 'json':
                return json($result);
            case 'jsonp':
                return jsonp($result, request()->input('callback'));
            default:
                $result['code'] = 0;
                $result['msg'] = '不支持的返回类型:' . $type;

                return json($result);
        }
    }
}